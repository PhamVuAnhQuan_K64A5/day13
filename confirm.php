<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array(
	"MAT" => "Khoa học máy tính",
	"KDL" => "Khoa học vật liệu"
);

session_start();

$username = $_SESSION["username"];
$gender = $genders[$_SESSION["gender"]];
$faculty = $faculties[$_SESSION["faculty"]];
$dob = $_SESSION["dob"];
$address = $_SESSION["address"];
$image = $_SESSION["image"];
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Xác nhận đăng ký</title>
</head>
<body>
    <form method="POST" action="complete_regist.php">
        <div class="info-field-div">
            <div class="label-div">
                <label for="name">Họ và tên</label>
            </div>
            <?php echo "<p>$username</p>"; ?>
        </div>
        <div class="info-field-div">
            <div class="label-div">
                <label for="gender">Giới tính</label>
            </div>
			<?php echo "<p>$gender</p>"; ?>
        </div>
		<div class="info-field-div">
            <div class="label-div">
                <label for="faculty">Phân khoa</label>
            </div>
			<?php echo "<p>$faculty</p>"; ?>
        </div>
		<div class="info-field-div">
			<div class="label-div">
				<label for="dob">Ngày sinh</label>
			</div>
			<?php echo "<p>$dob</p>"; ?>
		</div>
		<div class="info-field-div">
			<div class="label-div">
				<label for="address">Địa chỉ</label>
			</div>
			<?php echo "<p>$address</p>"; ?>
		</div>
		<div class="info-field-div image-view">
			<div class="label-div">
				<label for="image">Hình ảnh</label>
			</div>
			<?php 
				if (!empty($image)) {
					echo "<img src=\"$image\" width=\"100\" height=\"50\">";
				}
			?>
		</div>
        <div class="submit-div">
            <input type="submit" value="Xác nhận"></input>
        </div>
    </form>
</body>
</html>
