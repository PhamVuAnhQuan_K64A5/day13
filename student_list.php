<?php

function result_to_array($result) {
    $rows = [];
    while ($row = $result->fetch_row()) {
        $rows[] = $row;
    }
    return $rows;
}

function execute_select_query($query) {
    $conn = new mysqli("localhost", "root", "Root@mysql1", "qlsv");
    if ($conn->connect_errno) {
        echo "Failed to connect to MySQL: " . $conn->connect_error;
        exit();
    }
    $result = $conn->query($query);
    $conn->close();
    return $result;
}

function get_student_list($faculty, $keyword) {
    $sql_query = "select id, name, faculty from student where faculty like '%$faculty%' and name like '%$keyword%';";
    $result = execute_select_query($sql_query);
    $rows = result_to_array($result);
    return $rows;
}

$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array(
	"MAT" => "Khoa học máy tính",
	"KDL" => "Khoa học vật liệu"
);

$faculty_input_val = "";
$keyword_input_val = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    session_start();
    $_SESSION = $_POST;
    $faculty_input_val = htmlspecialchars($_SESSION["faculty"]);
    $keyword_input_val = htmlspecialchars($_SESSION["keyword"]);
}

$faculty_key = array_search($faculty_input_val, $faculties);
$number_of_student = count(get_student_list($faculty_key, $keyword_input_val));
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
    <title>Danh sách sinh viên</title>
</head>
<body>
    <form class="home" method="POST" action="">
        <div class="home-input-field-div">
            <div class="label-div-without-border-n-bg">
                <label for="faculty">Khoa</label>
            </div>
            <select name="faculty" id="faculty" class="m-input">
				<?php
					echo "<option></option>";
					foreach ($faculties as $faculty) {
                        $outp = "<option value=\"$faculty\">$faculty</option>";
                        if ($faculty == $faculty_input_val) {
                            $outp = "<option value=\"$faculty\" selected=\"selected\">$faculty</option>";
                        }
						echo $outp;
					}
				?>
			</select>
        </div>
        <div class="home-input-field-div">
            <div class="label-div-without-border-n-bg">
                <label for="keyword">Từ khoá</label>
            </div>
            <input class="home-text-input m-input" type="text" id="keyword" name="keyword" value="<?php echo $keyword_input_val ?>">
        </div>
        <div class="submit-div">
            <input type="submit" value="Tìm kiếm"></input>
            <input type="button" value="Xoá" id="delete-btn"></input>
        </div>
    </form>
    <div class="student-list">
        <div class="student-list-header">
            <div>
                <?php echo "<p>Số sinh viên tìm thấy: " . $number_of_student . "</p>"; ?>
            </div>
            <div>
                <input type="submit" value="Thêm" onclick="location.href='form.php'"/>
            </div>
        </div>

        <table>
            <tr>
                <td width="100">No</td>
                <td>Tên sinh viên</td>
                <td>Khoa</td>
                <td>Action</td>
            </tr>
            <?php
                $student_list = get_student_list($faculty_key, $keyword_input_val);
                foreach ($student_list as $student) {
                    echo "<tr>";
                    echo "<td>$student[0]</td>";
                    echo "<td>$student[1]</td>";
                    $faculty_of_std = $faculties[$student[2]];
                    echo "<td>$faculty_of_std</td>";
                    echo "<td><button>Xoá</button>  <button>Sửa</button></td>";
                    echo "</tr>";
                }
            ?>
        </table>
    </div>
</body>

<script>
    $(document).ready(function() {
        $("#delete-btn").click(function() {
            $(".m-input").val("");
        })
    })
</script>
</html>